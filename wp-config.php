<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/YEowW75XI0SKPVNaG9KVkBXBDRa0qd90dYceahh9KMHsa3EfKYqBEreD6qCCcVPo7D4HFqlzdWWYZJOy/jtug==');
define('SECURE_AUTH_KEY',  'YejqopCLfmfzLWqnjxyAcsAD8rbsxzSFRJdiaXTxNVVNIKi299eJwcGTviToLCga/XS/Gug0AWhPjo8FBMnNDA==');
define('LOGGED_IN_KEY',    '4XWhaj4/sTcSVGO0Vq+bPD8EPtEKwHZ4MT3OCCy4E5XMsNCwr9kAEgQcEHByKepWZQkFe2dNDs4QBAaq9sIejA==');
define('NONCE_KEY',        'vD5LsqQYjtfr4XdAV/AhXVUyoGV1NmIn8k62ea3iyjHujkVUiSYa29TyXQHQjyuApBceoY+apPcWwgQ6s46VDA==');
define('AUTH_SALT',        'tUvcMr1pwXDrsftPSthGVEDaUUvS9wYSkt/H+w+P8LCZvkFzGg2r38PNwBdiAp3sfqfHLdwjrHlQibp9NeFmrQ==');
define('SECURE_AUTH_SALT', 'JCCDQkANBTMsOixlmY19j/GCA+5hAIpFQ16g/UNMiM0C1hoSyPAKds6NIcCnHE7jazPTm9WAXtWKX7ba/BZE6g==');
define('LOGGED_IN_SALT',   'IUnJ7qLus1XwqEWVtgtGLmtNvJHq76Fl8vt24geud0Rthx+q5JlSHAhQgiCYiPGy/wz/KEKv1+jWCTBPCPVdGw==');
define('NONCE_SALT',       'PMPlh80wcgMd4jRFaMlIKPFwWx2TM97XJDCQkcC3uXJrmh3aPkEa3scK8FanPgL/rWIlk4Crwfg8bqAkI7xffg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
