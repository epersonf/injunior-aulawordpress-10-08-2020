
        <!-- Footer -->
        <footer>
            <nav>
                <div id="newsletter">
                    <div id="news-left">
                        <h3>Assine Nossa Newsletter</h3>
                        <h5>promoções e eventos mensais</h5>
                    </div>
                    <form id="news-right">
                        <input placeholder="Digite seu email"></input>
                        <button>ENVIAR</button>
                    </form>
                </div>
                <div id="info">
                    <p>Este é um projeto da Origamid. Mais em origamid.com<br>Praia de Botafogo, 300, 5º andar - Botafogo - Rio de Janeiro</p>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/brafe.png" alt="Logo da Brafé">
                </div>
            </nav>
        </footer>
    </body>
</html>