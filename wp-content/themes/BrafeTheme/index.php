<?php
    //Template Name: Home Page
?>

<?php
    echo get_header();
?>

<!-- Main -->
<main>
    
    <!-- SECTION 1-->
    <section class="container" id="cfs-br-sect1">
        <div id="innerText-sect1">
            <h3><?php the_field("titulo-chamativo"); ?></h3>
            <h5><?php the_field("subtitulo-chamativo"); ?></h5>
        </div>
    </section>
    
    <!-- SECTION 2-->
    <section class="container" id="cfs-br-sect2">
        <div id="m-mstr-d">
            <h3>Uma Mistura de</h3>
            <div id="sd-by-sd-imgs">
                <figure>
                    <img src="<?php the_field("imagem-produto-1"); ?>" alt="Imagem de café com coração feito de espuma.">
                    <figcaption>amor</figcaption>
                </figure>
                <figure>
                    <img src="<?php the_field("imagem-produto-2"); ?>" alt="Imagem de leite sendo colocado no café.">
                    <figcaption>perfeição</figcaption>
                </figure>
            </div>
            <p id="o-cf-e-txt">
            <?php the_field("texto_secao_produtos"); ?>
            </p>
        </div>
    </section>

    <!-- SECTION 3-->
    <section class="container" id="cfs-br-sect3">
        <div id="states">
            <div class="state-div">
                <h3 class="state-name" id="sp">Paulista</h3>
                <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</p>
            </div>
            
            <div class="state-div">
                <h3 class="state-name" id="rj">Carioca</h3>
                <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</p>
            </div>
            
            <div class="state-div">
                <h3 class="state-name" id="mg">Mineiro</h3>
                <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</p>
            </div>
        </div>
        <button>SAIBA MAIS</button>
    </section>

    <!-- SECTION 4 -->
    <section class="container" id="cfs-br-sect4">
        <div class="locations">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/botafogo.jpg" alt="Imagem de Botafogo">
            <div class="locationText">
                <h3>Botafogo</h3>
                <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
                <button class="seeMap">VER MAPA</button>
            </div>
        </div>
        <div class="locations">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/iguatemi.jpg" alt="Imagem de Iguatemi">
            <div class="locationText">
                <h3>Iguatemi</h3>
                <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
                <button class="seeMap">VER MAPA</button>
            </div>
        </div>
        <div class="locations">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/mineirao.jpg" alt="Imagem de Mineirao">
            <div class="locationText">
                <h3>Mineirao</h3>
                <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
                <button class="seeMap">VER MAPA</button>
            </div>
        </div>
    </section>
</main>

<?php
    echo get_footer();
?>