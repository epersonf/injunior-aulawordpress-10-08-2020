<?php
    // PHP functions

    function generate_title() {
        if (is_front_page() || is_404()) {
            echo "";
        } else {
            echo wp_title();
        }
    }

    function click_logo() {
        if (is_front_page()) {
            echo "#";
        } else {
            echo get_home_url();
        }
    }
?>

<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <!-- Page information -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            Brafé
            <?php
                generate_title();
            ?>
        </title>

        <!-- Reset always comes first -->
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/reset.css">

        <!-- CSS Injection -->
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">

        <!-- JavaScript Injection -->
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scriptfile.js"></script>
        <?php echo wp_head() ?>
    </head>

    <body>

        <!-- Header -->
        <header>
            <nav>
                <a href="<?php click_logo(); ?>"><h1>Brafé</h1></a>
                <?php
                    $args = array(
                        'menu' => 'principal',
                        'theme_location' => 'navigation',
                        'container' => false
                    );
                    echo wp_nav_menu();
                ?>
                <!-- <ul>
                    <li><a href="/sobre">Sobre</a></li>
                    <li><a href="/produtos">Produtos</a></li>
                    <li><a href="/portfolio">Portfólio</a></li>
                    <li><a href="/contato">Contato</a></li>
                </ul> -->
            </nav>
        </header>